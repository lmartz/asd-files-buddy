import typer
from pathlib import Path
from typing import Optional

app = typer.Typer()

def check_directory(directory: Optional[str]=None) -> Path:
    """Convert a given string into a path and checks if it exists on the current system.

    Args:
        path (str): A string representing a directory path.

    Raises:
        typer.Exit: If the path does not exist, raise an error and quits the script.

    Returns:
        Path: A pathlib object representing a path on the system
    """
    if directory:
        directory = Path(directory)
    else:
        directory = Path.cwd()
    if directory.exists():
        return directory
    else:
        typer.secho(f"The {directory}  directory does not exist", bold=True, fg=typer.colors.BRIGHT_MAGENTA)
        raise typer.Exit()

def file_glob(directory: Path) -> list:
    """Take a Path object as a parameter, recursively globes it to fetch all items with the asd extension in it.

    Args:
        directory (Path): A Path object existing on the current system

    Returns:
        list: A list containing all files with an '.asd' extension
    """
    return [asdfile for asdfile in directory.rglob("*.asd")]

def dispatch(delete: bool, directory: Optional[str]=None):
    """Main part of the job, calling the check_directory and file_glob funcs. Either list the files out of the list, or delete them.

    Args:
        directory (str): A string representing the directory to takes action in.
        delete (bool): A bool, if True, will delete the files fetched out of the file_glob func, if False, will list them to the user
    """
    directory = check_directory(directory)
    asd_files = file_glob(directory)
    file_count = len(asd_files)
    f_count = typer.style(file_count, bold=True, fg=typer.colors.BRIGHT_MAGENTA)
    if delete:
        if asd_files:
            typer.confirm(f"{file_count} file(s) have been found, do you really want to delete the found files ?", abort=True)
            with typer.progressbar(asd_files) as progress:
                for asdfile in progress:
                    try:
                        asdfile.unlink()
                    except FileExistsError as e:
                        typer.echo(f"Error: {e}")
            typer.echo(f"{f_count} files with the asd extension have been deleted")
        else:
            typer.echo("No asd files exist at the given path, nothing to delete.")
            raise typer.Exit()
    else:
        for asdfile in asd_files:
            print(asdfile)
        typer.echo(f"{f_count} files with the asd extension have been found")

@app.command('list')
def list_asd(directory: Optional[str] = typer.Argument(None, help="Directory to search in.")):
    """
    List all the asd files found at the given path.
    If no path is given, uses the current working directory.
    """
    dispatch(directory=directory, delete=False)

@app.command('delete')
def delete_asd(directory: Optional[str] = typer.Argument(None, help="Directory to search in..")):
    """
    Remove all the asd files found at the given path. If no path is given, uses the current working directory.
    """
    dispatch(directory=directory, delete=True)

if __name__ == "__main__":
    app()